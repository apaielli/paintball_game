// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_PAIELLI_Paintball_PaielliCharacter_generated_h
#error "Paintball_PaielliCharacter.generated.h already included, missing '#pragma once' in Paintball_PaielliCharacter.h"
#endif
#define PAINTBALL_PAIELLI_Paintball_PaielliCharacter_generated_h

#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_RPC_WRAPPERS
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliCharacter(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliCharacter(); \
public: \
	DECLARE_CLASS(APaintball_PaielliCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliCharacter(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliCharacter(); \
public: \
	DECLARE_CLASS(APaintball_PaielliCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_PaielliCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_PaielliCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliCharacter(APaintball_PaielliCharacter&&); \
	NO_API APaintball_PaielliCharacter(const APaintball_PaielliCharacter&); \
public:


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliCharacter(APaintball_PaielliCharacter&&); \
	NO_API APaintball_PaielliCharacter(const APaintball_PaielliCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_PaielliCharacter)


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintball_PaielliCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintball_PaielliCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintball_PaielliCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintball_PaielliCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintball_PaielliCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintball_PaielliCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintball_PaielliCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintball_PaielliCharacter, L_MotionController); }


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_15_PROLOG
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_RPC_WRAPPERS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_INCLASS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_INCLASS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
