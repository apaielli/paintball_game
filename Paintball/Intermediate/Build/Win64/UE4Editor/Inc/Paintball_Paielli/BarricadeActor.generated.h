// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_PAIELLI_BarricadeActor_generated_h
#error "BarricadeActor.generated.h already included, missing '#pragma once' in BarricadeActor.h"
#endif
#define PAINTBALL_PAIELLI_BarricadeActor_generated_h

#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_RPC_WRAPPERS
#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABarricadeActor(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_ABarricadeActor(); \
public: \
	DECLARE_CLASS(ABarricadeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(ABarricadeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABarricadeActor(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_ABarricadeActor(); \
public: \
	DECLARE_CLASS(ABarricadeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(ABarricadeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABarricadeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABarricadeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarricadeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarricadeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarricadeActor(ABarricadeActor&&); \
	NO_API ABarricadeActor(const ABarricadeActor&); \
public:


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarricadeActor(ABarricadeActor&&); \
	NO_API ABarricadeActor(const ABarricadeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarricadeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarricadeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABarricadeActor)


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneObject() { return STRUCT_OFFSET(ABarricadeActor, SceneObject); } \
	FORCEINLINE static uint32 __PPO__SceneMesh() { return STRUCT_OFFSET(ABarricadeActor, SceneMesh); } \
	FORCEINLINE static uint32 __PPO__ThisMat() { return STRUCT_OFFSET(ABarricadeActor, ThisMat); }


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_10_PROLOG
#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_RPC_WRAPPERS \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_INCLASS \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_INCLASS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Paielli_Source_Paintball_Paielli_BarricadeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
