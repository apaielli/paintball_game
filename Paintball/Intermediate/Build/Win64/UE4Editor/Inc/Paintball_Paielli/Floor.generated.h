// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_PAIELLI_Floor_generated_h
#error "Floor.generated.h already included, missing '#pragma once' in Floor.h"
#endif
#define PAINTBALL_PAIELLI_Floor_generated_h

#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_RPC_WRAPPERS
#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFloor(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_AFloor(); \
public: \
	DECLARE_CLASS(AFloor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(AFloor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFloor(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_AFloor(); \
public: \
	DECLARE_CLASS(AFloor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(AFloor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFloor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFloor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloor(AFloor&&); \
	NO_API AFloor(const AFloor&); \
public:


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloor(AFloor&&); \
	NO_API AFloor(const AFloor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFloor)


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneObject() { return STRUCT_OFFSET(AFloor, SceneObject); } \
	FORCEINLINE static uint32 __PPO__SceneMesh() { return STRUCT_OFFSET(AFloor, SceneMesh); } \
	FORCEINLINE static uint32 __PPO__ThisMat() { return STRUCT_OFFSET(AFloor, ThisMat); }


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_10_PROLOG
#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_RPC_WRAPPERS \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_INCLASS \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_INCLASS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Floor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Paielli_Source_Paintball_Paielli_Floor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
