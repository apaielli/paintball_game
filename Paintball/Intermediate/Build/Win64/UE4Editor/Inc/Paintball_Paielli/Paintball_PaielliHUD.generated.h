// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_PAIELLI_Paintball_PaielliHUD_generated_h
#error "Paintball_PaielliHUD.generated.h already included, missing '#pragma once' in Paintball_PaielliHUD.h"
#endif
#define PAINTBALL_PAIELLI_Paintball_PaielliHUD_generated_h

#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_RPC_WRAPPERS
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliHUD(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliHUD(); \
public: \
	DECLARE_CLASS(APaintball_PaielliHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliHUD(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliHUD(); \
public: \
	DECLARE_CLASS(APaintball_PaielliHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_PaielliHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_PaielliHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliHUD(APaintball_PaielliHUD&&); \
	NO_API APaintball_PaielliHUD(const APaintball_PaielliHUD&); \
public:


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliHUD(APaintball_PaielliHUD&&); \
	NO_API APaintball_PaielliHUD(const APaintball_PaielliHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_PaielliHUD)


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_9_PROLOG
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_RPC_WRAPPERS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_INCLASS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_INCLASS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
