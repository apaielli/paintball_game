// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBALL_PAIELLI_Paintball_PaielliProjectile_generated_h
#error "Paintball_PaielliProjectile.generated.h already included, missing '#pragma once' in Paintball_PaielliProjectile.h"
#endif
#define PAINTBALL_PAIELLI_Paintball_PaielliProjectile_generated_h

#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliProjectile(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliProjectile(); \
public: \
	DECLARE_CLASS(APaintball_PaielliProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_PaielliProjectile(); \
	friend PAINTBALL_PAIELLI_API class UClass* Z_Construct_UClass_APaintball_PaielliProjectile(); \
public: \
	DECLARE_CLASS(APaintball_PaielliProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Paielli"), NO_API) \
	DECLARE_SERIALIZER(APaintball_PaielliProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_PaielliProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_PaielliProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliProjectile(APaintball_PaielliProjectile&&); \
	NO_API APaintball_PaielliProjectile(const APaintball_PaielliProjectile&); \
public:


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_PaielliProjectile(APaintball_PaielliProjectile&&); \
	NO_API APaintball_PaielliProjectile(const APaintball_PaielliProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_PaielliProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_PaielliProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_PaielliProjectile)


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintball_PaielliProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintball_PaielliProjectile, ProjectileMovement); }


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_10_PROLOG
#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_RPC_WRAPPERS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_INCLASS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_INCLASS_NO_PURE_DECLS \
	Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Paintball_Paielli_Source_Paintball_Paielli_Paintball_PaielliProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
