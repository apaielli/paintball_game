// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Paintball_PaielliHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_PaielliHUD() {}
// Cross Module References
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APaintball_PaielliHUD_NoRegister();
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APaintball_PaielliHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Paintball_Paielli();
// End Cross Module References
	void APaintball_PaielliHUD::StaticRegisterNativesAPaintball_PaielliHUD()
	{
	}
	UClass* Z_Construct_UClass_APaintball_PaielliHUD_NoRegister()
	{
		return APaintball_PaielliHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintball_PaielliHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Paielli,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "Paintball_PaielliHUD.h" },
				{ "ModuleRelativePath", "Paintball_PaielliHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintball_PaielliHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintball_PaielliHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_PaielliHUD, 1424952311);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_PaielliHUD(Z_Construct_UClass_APaintball_PaielliHUD, &APaintball_PaielliHUD::StaticClass, TEXT("/Script/Paintball_Paielli"), TEXT("APaintball_PaielliHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_PaielliHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
