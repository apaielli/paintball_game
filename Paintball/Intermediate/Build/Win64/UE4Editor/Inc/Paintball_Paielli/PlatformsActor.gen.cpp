// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PlatformsActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlatformsActor() {}
// Cross Module References
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APlatformsActor_NoRegister();
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APlatformsActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Paielli();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void APlatformsActor::StaticRegisterNativesAPlatformsActor()
	{
	}
	UClass* Z_Construct_UClass_APlatformsActor_NoRegister()
	{
		return APlatformsActor::StaticClass();
	}
	UClass* Z_Construct_UClass_APlatformsActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Paielli,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "PlatformsActor.h" },
				{ "ModuleRelativePath", "PlatformsActor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisMat_MetaData[] = {
				{ "Category", "PlatformsActor" },
				{ "ModuleRelativePath", "PlatformsActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisMat = { UE4CodeGen_Private::EPropertyClass::Object, "ThisMat", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000020001, 1, nullptr, STRUCT_OFFSET(APlatformsActor, ThisMat), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(NewProp_ThisMat_MetaData, ARRAY_COUNT(NewProp_ThisMat_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMesh_MetaData[] = {
				{ "Category", "PlatformsActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "PlatformsActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneMesh = { UE4CodeGen_Private::EPropertyClass::Object, "SceneMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(APlatformsActor, SceneMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_SceneMesh_MetaData, ARRAY_COUNT(NewProp_SceneMesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneObject_MetaData[] = {
				{ "Category", "PlatformsActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "PlatformsActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneObject = { UE4CodeGen_Private::EPropertyClass::Object, "SceneObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(APlatformsActor, SceneObject), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(NewProp_SceneObject_MetaData, ARRAY_COUNT(NewProp_SceneObject_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisMat,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SceneMesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SceneObject,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APlatformsActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APlatformsActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlatformsActor, 881941379);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlatformsActor(Z_Construct_UClass_APlatformsActor, &APlatformsActor::StaticClass, TEXT("/Script/Paintball_Paielli"), TEXT("APlatformsActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlatformsActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
