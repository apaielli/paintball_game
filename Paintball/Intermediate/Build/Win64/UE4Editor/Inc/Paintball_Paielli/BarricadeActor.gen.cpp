// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "BarricadeActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBarricadeActor() {}
// Cross Module References
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_ABarricadeActor_NoRegister();
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_ABarricadeActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Paielli();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	RUNTIMEMESHCOMPONENT_API UClass* Z_Construct_UClass_URuntimeMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void ABarricadeActor::StaticRegisterNativesABarricadeActor()
	{
	}
	UClass* Z_Construct_UClass_ABarricadeActor_NoRegister()
	{
		return ABarricadeActor::StaticClass();
	}
	UClass* Z_Construct_UClass_ABarricadeActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Paielli,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "BarricadeActor.h" },
				{ "ModuleRelativePath", "BarricadeActor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThisMat_MetaData[] = {
				{ "Category", "BarricadeActor" },
				{ "ModuleRelativePath", "BarricadeActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisMat = { UE4CodeGen_Private::EPropertyClass::Object, "ThisMat", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000020001, 1, nullptr, STRUCT_OFFSET(ABarricadeActor, ThisMat), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(NewProp_ThisMat_MetaData, ARRAY_COUNT(NewProp_ThisMat_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMesh_MetaData[] = {
				{ "Category", "BarricadeActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "BarricadeActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneMesh = { UE4CodeGen_Private::EPropertyClass::Object, "SceneMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ABarricadeActor, SceneMesh), Z_Construct_UClass_URuntimeMeshComponent_NoRegister, METADATA_PARAMS(NewProp_SceneMesh_MetaData, ARRAY_COUNT(NewProp_SceneMesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneObject_MetaData[] = {
				{ "Category", "BarricadeActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "BarricadeActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneObject = { UE4CodeGen_Private::EPropertyClass::Object, "SceneObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x00200800000a0009, 1, nullptr, STRUCT_OFFSET(ABarricadeActor, SceneObject), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(NewProp_SceneObject_MetaData, ARRAY_COUNT(NewProp_SceneObject_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ThisMat,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SceneMesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_SceneObject,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ABarricadeActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ABarricadeActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABarricadeActor, 2462433941);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABarricadeActor(Z_Construct_UClass_ABarricadeActor, &ABarricadeActor::StaticClass, TEXT("/Script/Paintball_Paielli"), TEXT("ABarricadeActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABarricadeActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
