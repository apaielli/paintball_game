// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Paintball_PaielliGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_PaielliGameMode() {}
// Cross Module References
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APaintball_PaielliGameMode_NoRegister();
	PAINTBALL_PAIELLI_API UClass* Z_Construct_UClass_APaintball_PaielliGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Paintball_Paielli();
// End Cross Module References
	void APaintball_PaielliGameMode::StaticRegisterNativesAPaintball_PaielliGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaintball_PaielliGameMode_NoRegister()
	{
		return APaintball_PaielliGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintball_PaielliGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Paielli,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "Paintball_PaielliGameMode.h" },
				{ "ModuleRelativePath", "Paintball_PaielliGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintball_PaielliGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintball_PaielliGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_PaielliGameMode, 2960939921);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_PaielliGameMode(Z_Construct_UClass_APaintball_PaielliGameMode, &APaintball_PaielliGameMode::StaticClass, TEXT("/Script/Paintball_Paielli"), TEXT("APaintball_PaielliGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_PaielliGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
