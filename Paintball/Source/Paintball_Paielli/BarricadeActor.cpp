// Fill out your copyright notice in the Description page of Project Settings.

#include "BarricadeActor.h"


// Sets default values
ABarricadeActor::ABarricadeActor()
{
	SceneObject = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = SceneObject;

	SceneMesh = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("RuntimeMesh"));
	SceneMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UMaterial> asset(TEXT("Material'/Game/Shaders/Plank_MAT.Plank_MAT'"));
	ThisMat = asset.Object;

}

// Called when the game starts or when spawned
void ABarricadeActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABarricadeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABarricadeActor::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();
}

void ABarricadeActor::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();
}

//Function called to initialize variables used to generate mesh and call function taht populates variables and give mesh form
void ABarricadeActor::GenerateMesh()
{
	TArray < FVector > Vertices;
	TArray < FVector > Normals;
	TArray < FRuntimeMeshTangent > Tangents;
	TArray < FVector2D > TextureCoordinates;
	TArray < int32 > Triangles;
	TArray < FColor > Colors;

	CreateBarricadeMesh(Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);
    SceneMesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

//Function that assigns mesh variables and give mesh form
void ABarricadeActor::CreateBarricadeMesh(TArray<FVector>& Vertices, TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FRuntimeMeshTangent>& Tangents, TArray<FColor>& Colors)
{
	//Number of vertices in mesh
	FVector ObjectVerts[32];
	//Mesh Vertices
	ObjectVerts[0] = FVector(0.f, 0.f, 300.f);
	ObjectVerts[1] = FVector(0.f, 0.f, 200.f);
	ObjectVerts[2] = FVector(0.f, 100.f, 300.f);
	ObjectVerts[3] = FVector(0.f, 100.f, 200.f);
	ObjectVerts[4] = FVector(0.f, 200.f, 300.f);
	ObjectVerts[5] = FVector(0.f, 200.f, 200.f);
	ObjectVerts[6] = FVector(0.f, 300.f, 300.f);
	ObjectVerts[7] = FVector(0.f, 300.f, 200.f);
	ObjectVerts[8] = FVector(0.f, 0.f, 100.f);
	ObjectVerts[9] = FVector(0.f, 0.f, 0.f);
	ObjectVerts[10] = FVector(0.f, 100.f, 100.f);
	ObjectVerts[11] = FVector(0.f, 100.f, 0.f);
	ObjectVerts[12] = FVector(0.f, 200.f, 100.f);
	ObjectVerts[13] = FVector(0.f, 200.f, 0.f);
	ObjectVerts[14] = FVector(0.f, 300.f, 100.f);
	ObjectVerts[15] = FVector(0.f, 300.f, 0.f);
	
	ObjectVerts[16] = FVector(50.f, 300.f, 300.f);
	ObjectVerts[17] = FVector(50.f, 300.f, 200.f);
	ObjectVerts[18] = FVector(50.f, 200.f, 300.f);
	ObjectVerts[19] = FVector(50.f, 200.f, 200.f);
	ObjectVerts[20] = FVector(50.f, 100.f, 300.f);
	ObjectVerts[21] = FVector(50.f, 100.f, 200.f);
	ObjectVerts[22] = FVector(50.f, 0.f, 300.f);
	ObjectVerts[23] = FVector(50.f, 0.f, 200.f);
	ObjectVerts[24] = FVector(50.f, 300.f, 100.f);
	ObjectVerts[25] = FVector(50.f, 200.f, 100.f);
	ObjectVerts[26] = FVector(50.f, 100.f, 100.f);
	ObjectVerts[27] = FVector(50.f, 0.f, 100.f);
	ObjectVerts[28] = FVector(50.f, 300.f, 0.f);
	ObjectVerts[29] = FVector(50.f, 200.f, 0.f);
	ObjectVerts[30] = FVector(50.f, 100.f, 0.f);
	ObjectVerts[31] = FVector(50.f, 0.f, 0.f);
	
	Triangles.Reset();
	//Number of vertices perface by number of faces
	const int32 NumVerts = 128; 
	Colors.Reset();
	Colors.AddUninitialized(NumVerts);
	//Setup of initial mesh coloration (checkerboard pattern)
	for (int i = 0; i < NumVerts / 3; i++) {
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

   /*Front Side*/
	Vertices[0] = ObjectVerts[0];
	Vertices[1] = ObjectVerts[1];
	Vertices[2] = ObjectVerts[2];
	Vertices[3] = ObjectVerts[3];
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(1);
	Triangles.Add(3);
	Triangles.Add(2);
	Normals[0] = Normals[1] = Normals[2] = Normals[3] = FVector(0, -1, 0);
	Tangents[0] = Tangents[1] = Tangents[2] = Tangents[3] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[4] = ObjectVerts[2];
	Vertices[5] = ObjectVerts[3];
	Vertices[6] = ObjectVerts[4];
	Vertices[7] = ObjectVerts[5];
	Triangles.Add(4);
	Triangles.Add(5);
	Triangles.Add(6);
	Triangles.Add(5);
	Triangles.Add(7);
	Triangles.Add(6);
	Normals[4] = Normals[5] = Normals[6] = Normals[7] = FVector(0, -1, 0);
	Tangents[4] = Tangents[5] = Tangents[6] = Tangents[7] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[8] = ObjectVerts[4];
	Vertices[9] = ObjectVerts[5];
	Vertices[10] = ObjectVerts[6];
	Vertices[11] = ObjectVerts[7];
	Triangles.Add(8);
	Triangles.Add(9);
	Triangles.Add(10);
	Triangles.Add(9);
	Triangles.Add(11);
	Triangles.Add(10);
	Normals[8] = Normals[9] = Normals[10] = Normals[11] = FVector(0, -1, 0);
	Tangents[8] = Tangents[9] = Tangents[10] = Tangents[11] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[12] = ObjectVerts[1];
	Vertices[13] = ObjectVerts[8];
	Vertices[14] = ObjectVerts[3];
	Vertices[15] = ObjectVerts[10];
	Triangles.Add(12);
	Triangles.Add(13);
	Triangles.Add(14);
	Triangles.Add(13);
	Triangles.Add(15);
	Triangles.Add(14);
	Normals[12] = Normals[13] = Normals[14] = Normals[15] = FVector(0, -1, 0);
	Tangents[12] = Tangents[13] = Tangents[14] = Tangents[15] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[16] = ObjectVerts[5];
	Vertices[17] = ObjectVerts[12];
	Vertices[18] = ObjectVerts[7];
	Vertices[19] = ObjectVerts[14];
	Triangles.Add(16);
	Triangles.Add(17);
	Triangles.Add(18);
	Triangles.Add(17);
	Triangles.Add(19);
	Triangles.Add(18);
	Normals[16] = Normals[17] = Normals[18] = Normals[19] = FVector(0, -1, 0);
	Tangents[16] = Tangents[17] = Tangents[18] = Tangents[19] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[20] = ObjectVerts[8];
	Vertices[21] = ObjectVerts[9];
	Vertices[22] = ObjectVerts[10];
	Vertices[23] = ObjectVerts[11];
	Triangles.Add(20);
	Triangles.Add(21);
	Triangles.Add(22);
	Triangles.Add(21);
	Triangles.Add(23);
	Triangles.Add(22);
	Normals[20] = Normals[21] = Normals[22] = Normals[23] = FVector(0, -1, 0);
	Tangents[20] = Tangents[21] = Tangents[22] = Tangents[23] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[24] = ObjectVerts[10];
	Vertices[25] = ObjectVerts[11];
	Vertices[26] = ObjectVerts[12];
	Vertices[27] = ObjectVerts[13];
	Triangles.Add(24);
	Triangles.Add(25);
	Triangles.Add(26);
	Triangles.Add(25);
	Triangles.Add(27);
	Triangles.Add(26);
	Normals[24] = Normals[25] = Normals[26] = Normals[27] = FVector(0, -1, 0);
	Tangents[24] = Tangents[25] = Tangents[26] = Tangents[27] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[28] = ObjectVerts[12];
	Vertices[29] = ObjectVerts[13];
	Vertices[30] = ObjectVerts[14];
	Vertices[31] = ObjectVerts[15];
	Triangles.Add(28);
	Triangles.Add(29);
	Triangles.Add(30);
	Triangles.Add(29);
	Triangles.Add(31);
	Triangles.Add(30);
	Normals[28] = Normals[29] = Normals[30] = Normals[31] = FVector(0, -1, 0);
	Tangents[28] = Tangents[29] = Tangents[30] = Tangents[31] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Back Side*/
	Vertices[32] = ObjectVerts[16];
	Vertices[33] = ObjectVerts[17];
	Vertices[34] = ObjectVerts[18];
	Vertices[35] = ObjectVerts[19];
	Triangles.Add(32);
	Triangles.Add(33);
	Triangles.Add(34);
	Triangles.Add(33);
	Triangles.Add(35);
	Triangles.Add(34);
	Normals[32] = Normals[33] = Normals[34] = Normals[35] = FVector(0, 1, 0);
	Tangents[32] = Tangents[33] = Tangents[34] = Tangents[35] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[36] = ObjectVerts[18];
	Vertices[37] = ObjectVerts[19];
	Vertices[38] = ObjectVerts[20];
	Vertices[39] = ObjectVerts[21];
	Triangles.Add(36);
	Triangles.Add(37);
	Triangles.Add(38);
	Triangles.Add(37);
	Triangles.Add(39);
	Triangles.Add(38);
	Normals[36] = Normals[37] = Normals[38] = Normals[39] = FVector(0, 1, 0);
	Tangents[36] = Tangents[37] = Tangents[38] = Tangents[39] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[40] = ObjectVerts[20];
	Vertices[41] = ObjectVerts[21];
	Vertices[42] = ObjectVerts[22];
	Vertices[43] = ObjectVerts[23];
	Triangles.Add(40);
	Triangles.Add(41);
	Triangles.Add(42);
	Triangles.Add(41);
	Triangles.Add(43);
	Triangles.Add(42);
	Normals[40] = Normals[41] = Normals[42] = Normals[43] = FVector(0, 1, 0);
	Tangents[40] = Tangents[41] = Tangents[42] = Tangents[43] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[44] = ObjectVerts[17];
	Vertices[45] = ObjectVerts[24];
	Vertices[46] = ObjectVerts[19];
	Vertices[47] = ObjectVerts[25];
	Triangles.Add(44);
	Triangles.Add(45);
	Triangles.Add(46);
	Triangles.Add(45);
	Triangles.Add(47);
	Triangles.Add(46);
	Normals[44] = Normals[45] = Normals[46] = Normals[47] = FVector(0, 1, 0);
	Tangents[44] = Tangents[45] = Tangents[46] = Tangents[47] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[48] = ObjectVerts[21];
	Vertices[49] = ObjectVerts[26];
	Vertices[50] = ObjectVerts[23];
	Vertices[51] = ObjectVerts[27];
	Triangles.Add(48);
	Triangles.Add(49);
	Triangles.Add(50);
	Triangles.Add(49);
	Triangles.Add(51);
	Triangles.Add(50);
	Normals[48] = Normals[49] = Normals[50] = Normals[51] = FVector(0, 1, 0);
	Tangents[48] = Tangents[49] = Tangents[50] = Tangents[51] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[52] = ObjectVerts[24];
	Vertices[53] = ObjectVerts[28];
	Vertices[54] = ObjectVerts[25];
	Vertices[55] = ObjectVerts[29];
	Triangles.Add(52);
	Triangles.Add(53);
	Triangles.Add(54);
	Triangles.Add(53);
	Triangles.Add(55);
	Triangles.Add(54);
	Normals[52] = Normals[53] = Normals[54] = Normals[55] = FVector(0, 1, 0);
	Tangents[52] = Tangents[53] = Tangents[54] = Tangents[55] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[56] = ObjectVerts[25];
	Vertices[57] = ObjectVerts[29];
	Vertices[58] = ObjectVerts[26];
	Vertices[59] = ObjectVerts[30];
	Triangles.Add(56);
	Triangles.Add(57);
	Triangles.Add(58);
	Triangles.Add(57);
	Triangles.Add(59);
	Triangles.Add(58);
	Normals[56] = Normals[57] = Normals[58] = Normals[59] = FVector(0, 1, 0);
	Tangents[56] = Tangents[57] = Tangents[58] = Tangents[59] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[60] = ObjectVerts[26];
	Vertices[61] = ObjectVerts[30];
	Vertices[62] = ObjectVerts[27];
	Vertices[63] = ObjectVerts[31];
	Triangles.Add(60);
	Triangles.Add(61);
	Triangles.Add(62);
	Triangles.Add(61);
	Triangles.Add(63);
	Triangles.Add(62);
	Normals[60] = Normals[61] = Normals[62] = Normals[63] = FVector(0, 1, 0);
	Tangents[60] = Tangents[61] = Tangents[62] = Tangents[63] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Left Side*/
	Vertices[64] = ObjectVerts[22];
	Vertices[65] = ObjectVerts[23];
	Vertices[66] = ObjectVerts[0];
	Vertices[67] = ObjectVerts[1];
	Triangles.Add(64);
	Triangles.Add(65);
	Triangles.Add(66);
	Triangles.Add(65);
	Triangles.Add(67);
	Triangles.Add(66);
	Normals[64] = Normals[65] = Normals[66] = Normals[67] = FVector(-1, 0, 0);
	Tangents[64] = Tangents[65] = Tangents[66] = Tangents[67] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[68] = ObjectVerts[23];
	Vertices[69] = ObjectVerts[27];
	Vertices[70] = ObjectVerts[1];
	Vertices[71] = ObjectVerts[8];
	Triangles.Add(68);
	Triangles.Add(69);
	Triangles.Add(70);
	Triangles.Add(69);
	Triangles.Add(71);
	Triangles.Add(70);
	Normals[68] = Normals[69] = Normals[70] = Normals[71] = FVector(-1, 0, 0);
	Tangents[68] = Tangents[69] = Tangents[70] = Tangents[71] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[72] = ObjectVerts[27];
	Vertices[73] = ObjectVerts[31];
	Vertices[74] = ObjectVerts[8];
	Vertices[75] = ObjectVerts[9];
	Triangles.Add(72);
	Triangles.Add(73);
	Triangles.Add(74);
	Triangles.Add(73);
	Triangles.Add(75);
	Triangles.Add(74);
	Normals[72] = Normals[73] = Normals[74] = Normals[75] = FVector(-1, 0, 0);
	Tangents[72] = Tangents[73] = Tangents[74] = Tangents[75] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Inner Left*/
	Vertices[76] = ObjectVerts[19];
	Vertices[77] = ObjectVerts[25];
	Vertices[78] = ObjectVerts[5];
	Vertices[79] = ObjectVerts[12];
	Triangles.Add(76);
	Triangles.Add(77);
	Triangles.Add(78);
	Triangles.Add(77);
	Triangles.Add(79);
	Triangles.Add(78);
	Normals[76] = Normals[77] = Normals[78] = Normals[79] = FVector(-1, 0, 0);
	Tangents[76] = Tangents[77] = Tangents[78] = Tangents[79] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Right Side*/
	Vertices[80] = ObjectVerts[6];
	Vertices[81] = ObjectVerts[7];
	Vertices[82] = ObjectVerts[16];
	Vertices[83] = ObjectVerts[17];
	Triangles.Add(80);
	Triangles.Add(81);
	Triangles.Add(82);
	Triangles.Add(81);
	Triangles.Add(83);
	Triangles.Add(82);
	Normals[80] = Normals[81] = Normals[82] = Normals[83] = FVector(1, 0, 0);
	Tangents[80] = Tangents[81] = Tangents[82] = Tangents[83] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[84] = ObjectVerts[7];
	Vertices[85] = ObjectVerts[14];
	Vertices[86] = ObjectVerts[17];
	Vertices[87] = ObjectVerts[24];
	Triangles.Add(84);
	Triangles.Add(85);
	Triangles.Add(86);
	Triangles.Add(85);
	Triangles.Add(87);
	Triangles.Add(86);
	Normals[84] = Normals[85] = Normals[86] = Normals[87] = FVector(1, 0, 0);
	Tangents[84] = Tangents[85] = Tangents[86] = Tangents[87] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[88] = ObjectVerts[14];
	Vertices[89] = ObjectVerts[15];
	Vertices[90] = ObjectVerts[24];
	Vertices[91] = ObjectVerts[28];
	Triangles.Add(88);
	Triangles.Add(89);
	Triangles.Add(90);
	Triangles.Add(89);
	Triangles.Add(91);
	Triangles.Add(90);
	Normals[88] = Normals[89] = Normals[90] = Normals[91] = FVector(1, 0, 0);
	Tangents[88] = Tangents[89] = Tangents[90] = Tangents[91] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Inner Right*/
	Vertices[92] = ObjectVerts[3];
	Vertices[93] = ObjectVerts[10];
	Vertices[94] = ObjectVerts[21];
	Vertices[95] = ObjectVerts[26];
	Triangles.Add(92);
	Triangles.Add(93);
	Triangles.Add(94);
	Triangles.Add(93);
	Triangles.Add(95);
	Triangles.Add(94);
	Normals[92] = Normals[93] = Normals[94] = Normals[95] = FVector(1, 0, 0);
	Tangents[92] = Tangents[93] = Tangents[94] = Tangents[95] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Top Side*/
	Vertices[96] = ObjectVerts[6];
	Vertices[97] = ObjectVerts[16];
	Vertices[98] = ObjectVerts[4];
	Vertices[99] = ObjectVerts[18];
	Triangles.Add(96);
	Triangles.Add(97);
	Triangles.Add(98);
	Triangles.Add(97);
	Triangles.Add(99);
	Triangles.Add(98);
	Normals[96] = Normals[97] = Normals[98] = Normals[99] = FVector(0, 0, 1);
	Tangents[96] = Tangents[97] = Tangents[98] = Tangents[99] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[100] = ObjectVerts[4];
	Vertices[101] = ObjectVerts[18];
	Vertices[102] = ObjectVerts[2];
	Vertices[103] = ObjectVerts[20];
	Triangles.Add(100);
	Triangles.Add(101);
	Triangles.Add(102);
	Triangles.Add(101);
	Triangles.Add(103);
	Triangles.Add(102);
	Normals[100] = Normals[101] = Normals[102] = Normals[103] = FVector(0, 0, 1);
	Tangents[100] = Tangents[101] = Tangents[102] = Tangents[103] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	Vertices[104] = ObjectVerts[2];
	Vertices[105] = ObjectVerts[20];
	Vertices[106] = ObjectVerts[0];
	Vertices[107] = ObjectVerts[22];
	Triangles.Add(104);
	Triangles.Add(105);
	Triangles.Add(106);
	Triangles.Add(105);
	Triangles.Add(107);
	Triangles.Add(106);
	Normals[104] = Normals[105] = Normals[106] = Normals[107] = FVector(0, 0, 1);
	Tangents[104] = Tangents[105] = Tangents[106] = Tangents[107] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Inner Top*/
	Vertices[108] = ObjectVerts[26];
	Vertices[109] = ObjectVerts[10];
	Vertices[110] = ObjectVerts[25];
	Vertices[111] = ObjectVerts[12];
	Triangles.Add(108);
	Triangles.Add(109);
	Triangles.Add(110);
	Triangles.Add(109);
	Triangles.Add(111);
	Triangles.Add(110);
	Normals[108] = Normals[109] = Normals[110] = Normals[111] = FVector(0, 0, 1);
	Tangents[108] = Tangents[109] = Tangents[110] = Tangents[111] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Bottom Side*/
	Vertices[112] = ObjectVerts[28];
	Vertices[113] = ObjectVerts[15];
	Vertices[114] = ObjectVerts[29];
	Vertices[115] = ObjectVerts[13];
	Triangles.Add(112);
	Triangles.Add(113);
	Triangles.Add(114);
	Triangles.Add(113);
	Triangles.Add(115);
	Triangles.Add(114);
	Normals[112] = Normals[113] = Normals[114] = Normals[115] = FVector(0, 0, -1);
	Tangents[112] = Tangents[113] = Tangents[114] = Tangents[115] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	
	Vertices[116] = ObjectVerts[29];
	Vertices[117] = ObjectVerts[13];
	Vertices[118] = ObjectVerts[30];
	Vertices[119] = ObjectVerts[11];
	Triangles.Add(116);
	Triangles.Add(117);
	Triangles.Add(118);
	Triangles.Add(117);
	Triangles.Add(119);
	Triangles.Add(118);
	Normals[116] = Normals[117] = Normals[118] = Normals[119] = FVector(0, 0, -1);
	Tangents[116] = Tangents[117] = Tangents[118] = Tangents[119] = FRuntimeMeshTangent(0.f, -1.f, 0.f);


	Vertices[120] = ObjectVerts[30];
	Vertices[121] = ObjectVerts[11];
	Vertices[122] = ObjectVerts[31];
	Vertices[123] = ObjectVerts[9];
	Triangles.Add(120);
	Triangles.Add(121);
	Triangles.Add(122);
	Triangles.Add(121);
	Triangles.Add(123);
	Triangles.Add(122);
	Normals[120] = Normals[121] = Normals[122] = Normals[123] = FVector(0, 0, -1);
	Tangents[120] = Tangents[121] = Tangents[122] = Tangents[123] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	/*Inner Bottom*/
	Vertices[124] = ObjectVerts[19];
	Vertices[125] = ObjectVerts[5];
	Vertices[126] = ObjectVerts[21];
	Vertices[127] = ObjectVerts[3];
	Triangles.Add(124);
	Triangles.Add(125);
	Triangles.Add(126);
	Triangles.Add(125);
	Triangles.Add(127);
	Triangles.Add(126);
	Normals[124] = Normals[125] = Normals[126] = Normals[127] = FVector(0, 0, -1);
	Tangents[124] = Tangents[125] = Tangents[126] = Tangents[127] = FRuntimeMeshTangent(0.f, -1.f, 0.f);

	//Setup of UV layout
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = UVs[8] = UVs[12] = UVs[16] = UVs[20] = UVs[24] = UVs[28] = UVs[32] = UVs[36] = UVs[40] = UVs[44] = UVs[48] = UVs[52] = UVs[56] = UVs[60] = UVs[64] = UVs[68] = UVs[72] = UVs[76] = UVs[80] = UVs[84] = UVs[88] = UVs[92] = UVs[96] = UVs[100] = UVs[104] = UVs[108] = UVs[112] = UVs[116] = UVs[120] = UVs[124] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = UVs[9] = UVs[13] = UVs[17] = UVs[21] = UVs[25] = UVs[29] = UVs[33] = UVs[37] = UVs[41] = UVs[45] = UVs[49] = UVs[53] = UVs[57] = UVs[61] = UVs[65] = UVs[69] = UVs[73] = UVs[77] = UVs[81] = UVs[85] = UVs[89] = UVs[93] = UVs[97] = UVs[101] = UVs[105] = UVs[108] = UVs[113] = UVs[117] = UVs[121] = UVs[125] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = UVs[10] = UVs[14] = UVs[18] = UVs[22] = UVs[26] = UVs[30] = UVs[34] = UVs[38] = UVs[42] = UVs[46] = UVs[50] = UVs[54] = UVs[58] = UVs[62] = UVs[66] = UVs[70] = UVs[74] = UVs[78] = UVs[82] = UVs[86] = UVs[90] = UVs[94] = UVs[98] = UVs[102] = UVs[106] = UVs[110] = UVs[114] = UVs[118] = UVs[122] = UVs[126] = FVector2D(1.f, 0.f);
	UVs[3] = UVs[7] = UVs[11] = UVs[15] = UVs[19] = UVs[23] = UVs[27] = UVs[31] = UVs[35] = UVs[39] = UVs[43] = UVs[47] = UVs[51] = UVs[55] = UVs[59] = UVs[63] = UVs[67] = UVs[71] = UVs[75] = UVs[79] = UVs[83] = UVs[87] = UVs[91] = UVs[95] = UVs[99] = UVs[103] = UVs[107] = UVs[111] = UVs[115] = UVs[119] = UVs[123] = UVs[127] = FVector2D(1.f, 1.f);
	
	//Sets mesh material
	SceneMesh->SetMaterial(0, ThisMat);
}



