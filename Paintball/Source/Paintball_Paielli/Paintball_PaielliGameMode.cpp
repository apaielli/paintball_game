// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_PaielliGameMode.h"
#include "Paintball_PaielliHUD.h"
#include "Paintball_PaielliCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintball_PaielliGameMode::APaintball_PaielliGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_PaielliHUD::StaticClass();
}
