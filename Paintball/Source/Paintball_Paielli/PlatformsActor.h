// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "PlatformsActor.generated.h"

UCLASS()
class PAINTBALL_PAIELLI_API APlatformsActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformsActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* SceneObject;

	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * SceneMesh;

public:	

	virtual void Tick(float DeltaTime) override;

	virtual void PostActorCreated() override;

	virtual void PostLoad() override;

	virtual void GenerateMesh();

	virtual void CreateBarricadeMesh(TArray<FVector>& Vertices, TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FProcMeshTangent>& Tangents, TArray<FColor>& Colors);

private:

	UPROPERTY(VisibleAnywhere)
		UMaterialInterface * ThisMat;

};
