// Fill out your copyright notice in the Description page of Project Settings.

#include "Floor.h"
#include "ConstructorHelpers.h"
#include "Materials/MaterialInstanceConstant.h"

// Sets default values
AFloor::AFloor()
{
	//Setup Root Component
	SceneObject = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = SceneObject;

	//Setup prodedural mesh component
	SceneMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("RuntimeMesh"));
	SceneMesh->SetupAttachment(RootComponent);

	//Constructs material instance from asset in file directory
	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> asset(TEXT("MaterialInstanceConstant'/Game/Shaders/LAVA/Lava_MAT_Inst.Lava_MAT_Inst'"));
	//Assigns material instance objects to pointer
	ThisMat = asset.Object;
}

// Called when the game starts or when spawned
void AFloor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Function to display actor in level editor
void AFloor::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();
}

//Function to display object prior to load.
void AFloor::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();
}

//Function called to initialize variables used to generate mesh and call function taht populates variables and give mesh form
void AFloor::GenerateMesh()
{
	TArray < FVector > Vertices;
	TArray < FVector > Normals;
	TArray < FProcMeshTangent > Tangents;
	TArray < FVector2D > TextureCoordinates;
	TArray < int32 > Triangles;
	TArray < FColor > Colors;

	CreateBarricadeMesh(Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);
	SceneMesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

//Function that assigns mesh variables and give mesh form
void AFloor::CreateBarricadeMesh(TArray<FVector>& Vertices, TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs, TArray<FProcMeshTangent>& Tangents, TArray<FColor>& Colors)
{
	//Number of vertices in mesh
	FVector ObjectVerts[8];
	//Mesh vertices
	ObjectVerts[0] = FVector(0.f, 0.f, 10.f);
	ObjectVerts[1] = FVector(0.f, 0.f, 0.f);
	ObjectVerts[2] = FVector(0.f, 5000.f, 10.f);
	ObjectVerts[3] = FVector(0.f, 5000.f, 0.f);
	ObjectVerts[4] = FVector(5000.f, 5000.f, 10.f);
	ObjectVerts[5] = FVector(5000.f, 5000.f, 0.f);
	ObjectVerts[6] = FVector(5000.f, 0.f, 10.f);
	ObjectVerts[7] = FVector(5000.f, 0.f, 0.f);

	Triangles.Reset();
	//Number of vertices perface by number of faces
	const int32 NumVerts = 24;
	Colors.Reset();
	Colors.AddUninitialized(NumVerts);

	//Setup of initial mesh coloration (checkerboard pattern)
	for (int i = 0; i < NumVerts / 3; i++) {
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}
	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);
	//Front
	Vertices[0] = ObjectVerts[0];
	Vertices[1] = ObjectVerts[1];
	Vertices[2] = ObjectVerts[2];
	Vertices[3] = ObjectVerts[3];
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(1);
	Triangles.Add(3);
	Triangles.Add(2);
	Normals[0] = Normals[1] = Normals[2] = Normals[3] = FVector(0, -1, 0);
	Tangents[0] = Tangents[1] = Tangents[2] = Tangents[3] = FProcMeshTangent(0.f, -1.f, 0.f);
	//Right
	Vertices[4] = ObjectVerts[2];
	Vertices[5] = ObjectVerts[3];
	Vertices[6] = ObjectVerts[4];
	Vertices[7] = ObjectVerts[5];
	Triangles.Add(4);
	Triangles.Add(5);
	Triangles.Add(6);
	Triangles.Add(5);
	Triangles.Add(7);
	Triangles.Add(6);
	Normals[4] = Normals[5] = Normals[6] = Normals[7] = FVector(1, 0, 0);
	Tangents[4] = Tangents[5] = Tangents[6] = Tangents[7] = FProcMeshTangent(0.f, -1.f, 0.f);
	//Back
	Vertices[8] = ObjectVerts[4];
	Vertices[9] = ObjectVerts[5];
	Vertices[10] = ObjectVerts[6];
	Vertices[11] = ObjectVerts[7];
	Triangles.Add(8);
	Triangles.Add(9);
	Triangles.Add(10);
	Triangles.Add(9);
	Triangles.Add(11);
	Triangles.Add(10);
	Normals[8] = Normals[9] = Normals[10] = Normals[11] = FVector(0, 1, 0);
	Tangents[8] = Tangents[9] = Tangents[10] = Tangents[11] = FProcMeshTangent(0.f, -1.f, 0.f);
	//Left
	Vertices[12] = ObjectVerts[6];
	Vertices[13] = ObjectVerts[7];
	Vertices[14] = ObjectVerts[0];
	Vertices[15] = ObjectVerts[1];
	Triangles.Add(12);
	Triangles.Add(13);
	Triangles.Add(14);
	Triangles.Add(13);
	Triangles.Add(15);
	Triangles.Add(14);
	Normals[12] = Normals[13] = Normals[14] = Normals[15] = FVector(0, 1, 0);
	Tangents[12] = Tangents[13] = Tangents[14] = Tangents[15] = FProcMeshTangent(0.f, -1.f, 0.f);
	//Top
	Vertices[16] = ObjectVerts[0];
	Vertices[17] = ObjectVerts[2];
	Vertices[18] = ObjectVerts[6];
	Vertices[19] = ObjectVerts[4];
	Triangles.Add(16);
	Triangles.Add(17);
	Triangles.Add(18);
	Triangles.Add(17);
	Triangles.Add(19);
	Triangles.Add(18);
	Normals[16] = Normals[17] = Normals[18] = Normals[19] = FVector(0, 0, 1);
	Tangents[16] = Tangents[17] = Tangents[18] = Tangents[19] = FProcMeshTangent(0.f, -1.f, 0.f);
	//Bottom
	Vertices[20] = ObjectVerts[3];
	Vertices[21] = ObjectVerts[1];
	Vertices[22] = ObjectVerts[5];
	Vertices[23] = ObjectVerts[7];
	Triangles.Add(20);
	Triangles.Add(21);
	Triangles.Add(22);
	Triangles.Add(21);
	Triangles.Add(23);
	Triangles.Add(22);
	Normals[20] = Normals[21] = Normals[22] = Normals[23] = FVector(0, 0, -1);
	Tangents[20] = Tangents[21] = Tangents[22] = Tangents[23] = FProcMeshTangent(0.f, -1.f, 0.f);

	//Setup of UV layout
	UVs.AddUninitialized(NumVerts);
	UVs[0] = UVs[4] = UVs[8] = UVs[12] = UVs[16] = UVs[20] = FVector2D(0.f, 0.f);
	UVs[1] = UVs[5] = UVs[9] = UVs[13] = UVs[17] = UVs[21] = FVector2D(0.f, 1.f);
	UVs[2] = UVs[6] = UVs[10] = UVs[14] = UVs[18] = UVs[22] = FVector2D(1.f, 0.f);
	UVs[3] = UVs[7] = UVs[11] = UVs[15] = UVs[19] = UVs[23] = FVector2D(1.f, 1.f);

	//Sets mesh material
	SceneMesh->SetMaterial(0, ThisMat);
}

