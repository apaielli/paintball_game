// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_PaielliProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/World.h"
#include "Components/SphereComponent.h"

APaintball_PaielliProjectile::APaintball_PaielliProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &APaintball_PaielliProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 7000.f;
	ProjectileMovement->MaxSpeed = 7000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;

	// Die after 3 seconds by default
	InitialLifeSpan = 0.5f;
}

void APaintball_PaielliProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (PaintballMaterial != NULL) // If there is a material assigned
	{
		// Set decal pointer to paintball material that is select
		Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), PaintballMaterial, FVector(UKismetMathLibrary::RandomFloatInRange(25.f, 60.f)), Hit.Location, Hit.Normal.Rotation(), 5.f);
		// Create dynamic material from decam material
		MatInstance = Decal->CreateDynamicMaterialInstance();
		// Set material parameter for 1 of 4 quadrents of material texture (different splatter shapes
		MatInstance->SetScalarParameterValue("frame", UKismetMathLibrary::RandomIntegerInRange(0, 3));
		// Set material parameter for base color of paintball splat
		MatInstance->SetVectorParameterValue("Color", FLinearColor(UKismetMathLibrary::RandomIntegerInRange(0.f, 1.f), UKismetMathLibrary::RandomIntegerInRange(0.f, 1.f), UKismetMathLibrary::RandomIntegerInRange(0.f, 1.f), 1.f));
	}



	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		Destroy();
	}
}